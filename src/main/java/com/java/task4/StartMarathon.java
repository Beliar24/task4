package com.java.task4;

import com.java.task4.let.Let;
import com.java.task4.let.Treadmill;
import com.java.task4.let.Wall;
import com.java.task4.participant.Cat;
import com.java.task4.participant.Human;
import com.java.task4.participant.Participant;
import com.java.task4.participant.Robot;
import com.java.task4.service.Participation;

public class StartMarathon {
    public static void main(String[] args) {
        Participant[] participants = {new Robot("RX-5000",60, 60),
                new Human("Den", 18, 15),
                new Cat("Larisa", 17, 25)};
        Let[] lets = {new Wall(20), new Treadmill(20)};
        Participation startGame = new Participation();
        startGame.start(lets, participants);
    }
}
