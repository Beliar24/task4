package com.java.task4.geometric;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Circle implements Figure {

    private final double pi;
    private final double r;

    @Override
    public double square() {
        return pi * Math.pow(r, 2);
    }
}
