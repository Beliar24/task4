package com.java.task4.geometric;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Triangle implements Figure {

    private final double a;
    private final double h;

    @Override
    public double square() {
        return 0.5 * a * h;
    }
}
