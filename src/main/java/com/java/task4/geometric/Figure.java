package com.java.task4.geometric;

public interface Figure {

    double square();
}
