package com.java.task4.geometric;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class Square implements Figure {

    private final double a;

    @Override
    public double square() {
        return a * a;
    }
}
