package com.java.task4.service;

import com.java.task4.let.Let;
import com.java.task4.participant.Participant;

import java.util.ArrayList;
import java.util.List;

public class Participation {

    public void start(Let[] mass, Participant[] participants) {
        List<Participant> participantList = new ArrayList<>();
        for (Let let : mass) {
            if (let.overcome().equals("wall")) {
                participantList = checkEnduranceWall(participants, let);
            }
            if (let.overcome().equals("treadmill")) {
                checkEnduranceTreadmill(participantList, let);
            }
        }
    }

    private List<Participant> checkEnduranceWall(Participant[] participants, Let let) {
        List<Participant> winners = new ArrayList<>();
        for (Participant participant : participants) {
            if (participant.getEnduranceJumping() - let.getDistance() < 0) {
                System.out.printf("The participant %s don't pass let %s for distance %s.Passed %s",
                        participant.getName(), let.overcome(), let.getDistance(), participant.getEnduranceJumping() + "\n");
            }
            if (participant.getEnduranceJumping() - let.getDistance() > 0) {
                jumping(participant, let);
                winners.add(participant);
            }
        }
        return winners;
    }

    private void checkEnduranceTreadmill(List<Participant> participants, Let let) {
        for (Participant participant : participants) {
            if (participant.getEnduranceRunning() - let.getDistance() < 0) {
                System.out.printf("The participant %s don't pass let %s for distance %s.Passed %s",
                        participant.getName(), let.overcome(), let.getDistance(), participant.getEnduranceRunning() + "\n");
            }
            if (participant.getEnduranceRunning() - let.getDistance() > 0) {
                running(participant, let);
            }
        }
    }


    private void running(Participant participant, Let let) {
        participant.run(let);
    }

    private void jumping(Participant participant, Let let) {
        participant.jump(let);
    }
}
