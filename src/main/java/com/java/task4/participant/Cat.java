package com.java.task4.participant;

import com.java.task4.let.Let;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cat extends Participant {


    public Cat(String name, int enduranceRunning, int enduranceJumping) {
        super(name, enduranceRunning, enduranceJumping);
    }

    @Override
    public void run(Let let) {
        System.out.printf("The cat %s running on %s", getName(), let.overcome()
                + " for distance " + let.getDistance() + "\n");
    }

    @Override
    public void jump(Let let) {
        System.out.printf("The cat %s jumping across %s", getName(), let.overcome()
                + " for distance " + let.getDistance() + "\n");
    }
}
