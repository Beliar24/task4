package com.java.task4.participant;

import com.java.task4.let.Let;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public abstract class Participant {

    private final String name;
    private final int enduranceRunning;
    private final int enduranceJumping;

    public abstract void run(Let let);

    public abstract void jump(Let let);
}
