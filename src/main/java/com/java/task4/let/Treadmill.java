package com.java.task4.let;

import lombok.Getter;

@Getter
public class Treadmill extends Let {

    public Treadmill(int distance) {
        super(distance);
    }

    @Override
    public String overcome() {
        return "treadmill";
    }
}
