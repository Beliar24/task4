package com.java.task4.let;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public abstract class Let {

    private final int distance;

    public abstract String overcome();
}
