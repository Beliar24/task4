package com.java.task4.let;

import lombok.Getter;

@Getter
public class Wall extends Let {

    public Wall(int distance) {
        super(distance);
    }

    @Override
    public String overcome() {
        return "wall";
    }
}
