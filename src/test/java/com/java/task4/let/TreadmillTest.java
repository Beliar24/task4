package com.java.task4.let;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TreadmillTest {

    Let treadmill = new Treadmill(20);

    @Test
    void shouldBeCorrectOutputMethodOvercome() {
        assertEquals(treadmill.overcome(), "treadmill");
    }

    @Test
    void shouldBeCorrectDistance() {
        assertEquals(treadmill.getDistance(), 20);
    }

}