package com.java.task4.let;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WallTest {

    Let wall = new Wall(10);

    @Test
    void shouldBeCorrectOutputMethodOvercome() {
        assertEquals(wall.overcome(), "wall");
    }

    @Test
    void shouldBeCorrectDistance() {
        assertEquals(wall.getDistance(), 10);
    }

}