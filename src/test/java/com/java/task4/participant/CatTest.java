package com.java.task4.participant;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CatTest {

    Participant cat = new Cat("Lori", 15, 20);

    @Test
    void shouldBeCorrectName() {
        assertEquals(cat.getName(), "Lori");
    }

    @Test
    void shouldBeCorrectEnduranceRunning() {
        assertEquals(cat.getEnduranceRunning(), 15);
    }

    @Test
    void shouldBeCorrectEnduranceWall() {
        assertEquals(cat.getEnduranceJumping(), 20);
    }

}