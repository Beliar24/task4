package com.java.task4.participant;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RobotTest {

    Participant robot = new Robot("RX-5000", 60, 65);

    @Test
    void shouldBeCorrectName() {
        assertEquals(robot.getName(), "RX-5000");
    }

    @Test
    void shouldBeCorrectEnduranceRunning() {
        assertEquals(robot.getEnduranceRunning(), 60);
    }

    @Test
    void shouldBeCorrectEnduranceWall() {
        assertEquals(robot.getEnduranceJumping(), 65);
    }

}