package com.java.task4.participant;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    Participant human = new Human("Den", 20, 25);

    @Test
    void shouldBeCorrectName() {
        assertEquals(human.getName(), "Den");
    }

    @Test
    void shouldBeCorrectEnduranceRunning() {
        assertEquals(human.getEnduranceRunning(), 20);
    }

    @Test
    void shouldBeCorrectEnduranceWall() {
        assertEquals(human.getEnduranceJumping(), 25);
    }


}