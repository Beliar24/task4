package com.java.task4.geometric;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TriangleTest {

    Figure triangle = new Triangle(5, 13);

    @Test
    void shouldBeCorrectSquareTriangle() {
        assertEquals(triangle.square(), 32.5);
    }

}