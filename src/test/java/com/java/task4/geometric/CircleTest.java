package com.java.task4.geometric;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CircleTest {

    Figure circle = new Circle(3.14, 6.0);

    @Test
    void shouldBeCorrectSquareCircle() {
        assertEquals(circle.square(), 113.04);
    }

}