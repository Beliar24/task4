package com.java.task4.geometric;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquareTest {

    Figure square = new Square(5.5);

    @Test
    void shouldBeCorrectSquare() {
        assertEquals(square.square(), 30.25);
    }

}